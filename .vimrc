execute pathogen#infect()

" Global Settings.
filetype plugin indent on
syntax on
set number
set hlsearch
set colorcolumn=80
" http://tedlogan.com/techblog3.html
set expandtab
set shiftwidth=2
set tabstop=2
set softtabstop=2

" Vim Airline.
set laststatus=2 " Make it appear from the start.
let g:airline_theme='solarized' " Use the Solarized Airline theme.

" Solarized theme.
set background=dark
colorscheme solarized

" GitGutter Dev suggested this.
set updatetime=250
